package br.com.teste.concretsolution.delegate;


import br.com.teste.concretsolution.model.Shots;

public interface ClickItemRecyclerViewOnListener {
    void onItemClicked(Shots shot);
}
