package br.com.teste.concretsolution.delegate;

public interface LoadListOnListener {
    void loadList();
}
