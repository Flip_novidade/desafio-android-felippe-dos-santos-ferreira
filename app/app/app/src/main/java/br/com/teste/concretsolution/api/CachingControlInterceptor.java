package br.com.teste.concretsolution.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by flip_novidade on 9/24/16.
 */
public class CachingControlInterceptor implements Interceptor {
    long SIZE_OF_CACHE = 10 * 1024 * 1024;
    private Context context;

    public CachingControlInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if (request.method().equals("GET")) {
            if (isConnected(context)) {

                request = request.newBuilder().header("Cache-Control", "only-if-cached").build();
            } else {

                request = request.newBuilder().header("Cache-Control", "public, max-stale="+SIZE_OF_CACHE).build();
            }
        }

        Response originalResponse = chain.proceed(request);
        return originalResponse.newBuilder().header("Cache-Control", "max-age=600").build();
    }

    public final static boolean isConnected( Context context ){
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
